from pathlib import Path

from conans import ConanFile, AutoToolsBuildEnvironment, tools


class CoinclpConan(ConanFile):
    name = "coincgl"
    version = "0.60.3"
    license = "EPL-1.0"
    author = "Harald Held <harald.held@gmail.com>"
    url = "https://gitlab.com/coinor-conan/coincgl-conan"
    description = "The COIN-OR Cut Generation Library (Cgl) is a collection of cut generators that can be used with other COIN-OR packages that make use of cuts."
    topics = ("Coin-OR", "cut generation", "optimization")
    settings = "os", "compiler", "build_type", "arch"
    generators = "pkg_config"
    requires = ["coinutils/2.11.4", "coinosi/0.108.6", "coinclp/1.17.6"]

    def source(self):
        self.run("git clone -b releases/0.60.3 https://github.com/coin-or/Cgl.git .")

    def build(self):
        autotools = AutoToolsBuildEnvironment(self)
        autotools.configure()
        autotools.make()
        autotools.install()

    def package_info(self):
        with tools.environment_append(
                {'PKG_CONFIG_PATH': [(Path(self.package_folder) / "lib" / "pkgconfig").as_posix()] + [p for dep in
                                                                                                      self.deps_cpp_info.deps
                                                                                                      for p in
                                                                                                      self.deps_env_info[
                                                                                                          dep].PKG_CONFIG_PATH]}):
            pkg_config = tools.PkgConfig("cgl")

            # strip -l, -L
            self.cpp_info.libs = [lib[2:] for lib in pkg_config.libs_only_l]
            self.cpp_info.libdirs = [libdir[2:] for libdir in pkg_config.libs_only_L]

            self.cpp_info.cxxflags = pkg_config.cflags

            self.env_info.PKG_CONFIG_PATH.append((Path(self.package_folder) / "lib" / "pkgconfig").as_posix())
